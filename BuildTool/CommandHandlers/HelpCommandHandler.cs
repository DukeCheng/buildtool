﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildTool.CommandHandlers
{
    [CommandHandlerName("help")]
    public class HelpCommandHandler : BaseCommandHandler
    {
        public override string GetResult(params string[] args)
        {
            StringBuilder helpMessage = new StringBuilder();
            foreach (var item in CommandHandlerFactory.GetAllCommandHandlerType())
            {
                if (Type.Equals(typeof(HelpCommandHandler), item))
                {
                    //ignore the HelpCommandHandler 
                    continue;
                }
                var commandHandler = Activator.CreateInstance(item) as ICommandHandler;
                helpMessage.Append(commandHandler.GetHelpMessage());
                helpMessage.AppendLine();
            }
            return helpMessage.ToString();
        }
    }
}
