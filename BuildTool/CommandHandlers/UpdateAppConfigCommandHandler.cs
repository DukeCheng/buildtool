﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BuildTool.CommandHandlers
{
    [CommandHandlerName("UpdateAppConfig")]
    public class UpdateAppConfigCommandHandler : BaseCommandHandler
    {
        public override string GetResult(params string[] args)
        {
            Console.WriteLine("args:");
            foreach (var item in args)
            {
                Console.WriteLine($"arg:{item}");
            }
            var filePath = GetArgumentsValue($"FilePath", args);
            var replaceXPath = GetArgumentsValue($"replaceXPath", args);

            Console.WriteLine($"parms: {filePath} {replaceXPath}");

            var configKey = replaceXPath.Substring(0, replaceXPath.IndexOf("=")).Trim();
            var configValue = replaceXPath.Substring(replaceXPath.IndexOf("=") + 1).Trim();
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            Console.WriteLine($"start try to update the connection string {configKey} to {configValue}");

            var node = doc.SelectSingleNode($"/configuration/connectionStrings/add[@name='{configKey}']");
            node.Attributes["connectionString"].Value = configValue;
            doc.Save(filePath);
            return string.Empty;
        }

        public override string GetHelpMessage()
        {
            return base.GetHelpMessage();
        }
    }
}
