﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Diagnostics;

namespace BuildTool.CommandHandlers
{
    internal class CommandHandlerFactory
    {
        private static Dictionary<string, Type> eventHandlerTypeMappingDic = new Dictionary<string, Type>();

        static CommandHandlerFactory()
        {
            InitCommandHandlerMaping();
        }

        internal static ICommandHandler CreateCommandHandler(string command, params string[] args)
        {
            var commandKey = command.ToLower().Trim();
            if (eventHandlerTypeMappingDic.ContainsKey(commandKey))
            {
                return Activator.CreateInstance(eventHandlerTypeMappingDic[commandKey]) as ICommandHandler;
            }
            return null;
        }

        private static void InitCommandHandlerMaping()
        {
            Assembly ass = Assembly.GetAssembly(typeof(ICommandHandler));
            var attrType = typeof(CommandHandlerNameAttribute);
            var baseHandlerType = typeof(BaseCommandHandler);
            var handlerTypes = ass.GetExportedTypes().Where(x => x.IsSubclassOf(baseHandlerType) && x.CustomAttributes.Any(attr => attr.AttributeType == attrType));
            foreach (Type targetType in handlerTypes)
            {
                var attr = targetType.GetCustomAttribute(attrType) as CommandHandlerNameAttribute;
                foreach (var commandKey in attr.HandlerName)
                {
                    if (!eventHandlerTypeMappingDic.ContainsKey(commandKey))
                    {
                        eventHandlerTypeMappingDic.Add(commandKey.Trim().ToLower(), targetType);
                    }

                }
            }
        }

        public static IReadOnlyList<Type> GetAllCommandHandlerType()
        {
            return eventHandlerTypeMappingDic.Select(x => x.Value).ToList().AsReadOnly();
        }
    }

    public interface ICommandHandler
    {
        string GetResult(params string[] args);
        string GetHelpMessage();
    }

    public abstract class BaseCommandHandler : ICommandHandler
    {
        public abstract string GetResult(params string[] args);
        public virtual string GetHelpMessage()
        {
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.AppendLine($"No HelpMessage for {this.GetType().FullName}");
            return sbMessage.ToString();
        }

        protected string GetArgumentsValue(string arguName, string[] args)
        {
            var result = args.FirstOrDefault(x => x.StartsWith($"-{arguName}", StringComparison.OrdinalIgnoreCase));
            if (string.IsNullOrWhiteSpace(result))
                return string.Empty;

            return result.Substring(result.IndexOf("=") + 1).Trim();
        }

        public Process CallCommand(string commandName, string parameters)
        {
            Console.WriteLine($"Start Call {commandName} {parameters}");
            var process = Process.Start(commandName, parameters);
            return process;
        }
    }

    public class CommandHandlerNameAttribute : Attribute
    {
        public string[] HandlerName { get; set; }
        public CommandHandlerNameAttribute(params string[] handlerName)
        {
            this.HandlerName = handlerName;
        }
    }
}
