﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BuildTool.CommandHandlers
{
    [CommandHandlerName("install")]
    public class InstallCommandHandler : BaseCommandHandler
    {
        public override string GetResult(params string[] args)
        {
            //if (Environment.OSVersion.Platform != PlatformID.Unix)
            //{
            //    return "Install Command Only suport unit system";
            //}

            Console.WriteLine("Prepare Create install folder");
            var installPath = @"/usr/lib/wwttools/";
            if (!Directory.Exists(installPath))
            {
                Console.WriteLine($"Create wwttools folder, path:{installPath}");
                Directory.CreateDirectory(installPath);
            }


            var currentPath = Assembly.GetEntryAssembly().Location;
            Console.WriteLine($"Prepare copy exe file {currentPath}");

            //            File.Copy(currentPath, Path.Combine(installPath, "BuildTool.exe"), true);
           CallCommand("cp", $"{new FileInfo(currentPath).Directory.FullName}/* {installPath}/").WaitForExit();

            Console.WriteLine($"Copy {new FileInfo(currentPath).Directory.FullName} to {installPath} success");

            Console.WriteLine("Prepare delete old file");
            var commandFile = "/usr/bin/buildtool";
            if (File.Exists(commandFile))
                File.Delete(commandFile);

            Console.WriteLine("Prepare Create command file");
            var fileStream = new FileStream(commandFile, FileMode.OpenOrCreate);
            using (var stream = new StreamWriter(fileStream, Encoding.UTF8))
            {
                stream.WriteLine(@"#! /bin/sh");
                stream.WriteLine("exec /usr/bin/cli /usr/lib/wwttools/BuildTool.exe \"$@\"");
            }

            Console.WriteLine($"Create execute file {commandFile} success");

            CallCommand("chmod", $"777 {commandFile}");
            Console.WriteLine($"Set {commandFile} permission success");

            return string.Empty;
        }
    }
}
