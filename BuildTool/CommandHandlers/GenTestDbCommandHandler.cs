﻿using Dapper;
using Npgsql;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace BuildTool.CommandHandlers
{
    [CommandHandlerName("GenTestDb", "GenerateTestDb", "gentestdb")]
    public class GenTestDbCommandHandler : BaseCommandHandler
    {
        public const string IntegrationTestDbNamePrefix = "qin_intgtestdb_";
        public override string GetResult(params string[] args)
        {
            var dbConnString = GetArgumentsValue("connectionstring", args);
            var dbConnFile = GetArgumentsValue("newConnStringOutputFile", args);
            var tempPath = GetArgumentsValue("TempPath", args);

            var conn = OpenConnection(dbConnString);

            var result = conn.Query<string>($" SELECT datname FROM pg_database where datname like '{IntegrationTestDbNamePrefix}%'").ToList();
            var newDatabaseName = string.Empty;
            for (int i = 1; i < 100; i++)
            {
                if (result.Contains($"{IntegrationTestDbNamePrefix}{i}"))
                {
                    continue;
                }

                newDatabaseName = $"{IntegrationTestDbNamePrefix}{i}";
                break;
            }

            NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder(dbConnString);
            builder.Database = newDatabaseName;
            var newConnectionString = builder.ConnectionString;

            Console.WriteLine("Start Craete New Database");
            CreateNewDatabase(dbConnString, newDatabaseName);

            Console.WriteLine($"Start update Db Schecma for new database, new database ConnString:{newConnectionString}, new DatabaseName:{newDatabaseName} ");
            CreateDatabaseTables(newConnectionString, Path.Combine(tempPath, "import"));//Import data
            CreateDatabaseTables(newConnectionString, tempPath);//create table schema
            CreateDatabaseTables(newConnectionString, Path.Combine(tempPath, "init-data"));//init application needed data
            CreateDatabaseTables(newConnectionString, Path.Combine(tempPath, "test-data"));//init test data

            Console.WriteLine("Prepare Create connection config file");
            var fileStream = new FileStream(dbConnFile, FileMode.OpenOrCreate);
            using (var stream = new StreamWriter(fileStream, Encoding.UTF8))
            {
                stream.WriteLine(newConnectionString);
            }

            return newConnectionString;
        }

        private void CreateDatabaseTables(string newConnectionString, string sqlScriptsDirectory)
        {
            //var process = CallCommand(Environment.OSVersion.Platform == PlatformID.Unix
            //    ? @"/usr/bin/git"
            //    : @"C:\Program Files\Git\cmd\git.exe ", $"clone git@bitbucket.org:wendywutoursdev/qin.database.git {sourceTarget}");

            //var exResult = process.WaitForExit(10 * 60 * 1000);

            using (var conn = OpenConnection(newConnectionString))
            {
                var sqlFiles = Directory.GetFiles(sqlScriptsDirectory, "*.sql");
                Console.WriteLine($"Total Found {sqlFiles.Count()} sql files");
                foreach (var sqlFile in sqlFiles)
                {
                    Console.WriteLine($"Start import file {sqlFile}");
                    conn.Execute(File.ReadAllText(sqlFile));
                }
            }
        }

        private void CreateNewDatabase(string connectionString, string newDatabaseName)
        {
            var dbCreateSql = $@"CREATE DATABASE {newDatabaseName}
  WITH ENCODING='UTF8'
       OWNER=wwwtqin
       CONNECTION LIMIT=-1;";

            using (var conn = OpenConnection(connectionString))
            {
                conn.Execute(dbCreateSql);
            }
        }

        protected IDbConnection OpenConnection(string connectionString = null)
        {
            Console.WriteLine($"Start Open Connection use ConnString:{connectionString}");
            var conn = new NpgsqlConnection(connectionString);
            try
            {
                conn.Open();
            }
            catch (PostgresException e)
            {
                //if (e.SqlState == "3D000")
                //   // TestUtil.IgnoreExceptOnBuildServer("Please create a database , owned by user npgsql_tests");
                //else if (e.SqlState == "28P01")
                //   // TestUtil.IgnoreExceptOnBuildServer("Please create a user  as follows: create user npgsql_tests with password 'npgsql_tests'");
                //else
                //    throw;
                Console.WriteLine($"Open DbConnection Failure, Reason: {e.Message}");
                throw;
            }

            return conn;
        }


    }
}
