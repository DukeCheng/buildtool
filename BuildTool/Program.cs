﻿using BuildTool.CommandHandlers;
using System;
using System.Linq;

namespace BuildTool
{
    class Program
    {
        public static int Main(params string[] args)
        {
            try
            {
                if (args == null || args.Length < 1)
                {
                    Console.Write(CommandHandlerFactory.CreateCommandHandler("help").GetResult());
                    return -1;
                }
                var command = args[0];
                var commandParms = args.ToList();
                if (commandParms.Count > 1)
                {
                    commandParms.RemoveAt(0);
                }
                var commandHandler = CommandHandlerFactory.CreateCommandHandler(command, commandParms.ToArray());
                Console.WriteLine(commandHandler.GetResult(commandParms.ToArray()));
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            return 1;
        }
    }
}
